<?php namespace Colloquy;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\User as UserContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements UserContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = ['password', 'remember_token'];
	protected $fillable = ['email', 'password'];

	/**
     * Get the roles a user has
     */
    public function roles()
    {
        return $this->belongsToMany('Colloquy\Role');
    }

    /**
     * Find out if user has a specific role
     *
     * $return boolean
     */
    public function hasRole($check)
    {
        return in_array($check, array_fetch($this->roles->toArray(), 'name'));
    }

    /**
     * Get the roles a user has
     */
    public function can($check)
    {
        // Foreach role check all the permissions and whether the user 
        // can perform the action
        foreach($this->roles as $role){
           if($this->hasRole($role->name)){
                foreach($role->permissions as $permission){
                    if($permission->name === $check){
                        return true;
                    }
                }
           }
        }
        return false;
    }



}
