<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Colloquy\Role as Role;
use Colloquy\Permission as Permission;

class RoleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$admin = new Role();
		$admin->name = 'Admin';
		$admin->save();

		$manage_users = new Permission();
		$manage_users->name = 'manage_users';
		$manage_users->save();

		$manage_posts = new Permission();
		$manage_posts->name = 'manage_posts';
		$manage_posts->save();

		$manage_roles = new Permission();
		$manage_roles->name = 'manage_roles';
		$manage_roles->save();

		$moderator = new Role();
		$moderator->name = 'Moderator';
		$moderator->save();

		$admin->permissions()->attach($manage_users->id);
		$admin->permissions()->attach($manage_posts->id);
		$admin->permissions()->attach($manage_roles->id);

		$moderator->permissions()->attach($manage_posts->id);
	}

}
