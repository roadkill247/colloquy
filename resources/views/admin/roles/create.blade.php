@extends('layouts.master')

@section('page_title', 'Create Role')

@section('content')

	<h3>Create Role</h3>

	{!! Form::open(['url' => 'admin/roles/create', 'method' => 'POST', 'class' => 'form'] ) !!}

	<div class="form-group">
		{!! Form::label('name', 'Name') !!}
		{!! Form::text('name', '', ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('permissions', 'Permissions:') !!}
		@foreach($permissions as $key => $permission)
			<li>{!! $permission->name !!} {!! Form::checkbox('permission['.$key.']', $permission->id) !!}</li>
		@endforeach
	</div>

	{!! Form::submit('Create', ['class' => 'btn']) !!}
	{!! Form::close() !!}

@stop

@section('javascript')

@stop