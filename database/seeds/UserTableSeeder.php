<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Colloquy\User as User;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$admin = new User();
		$admin->email = 'admin@admin.com';
		$admin->password = \Hash::make('password');
		$admin->save();

		$admin->roles()->attach(1);
		$admin->roles()->attach(2);
		
		$user = new User();
		$user->email = 'user@user.com';
		$user->password = \Hash::make('password');
		$user->save();

		$faker = Faker\Factory::create();
 
		for ($i = 0; $i < 10; $i++){
		  $user = User::create(array('email' => $faker->email,'password' => \Hash::make($faker->word)));
		}
	}

}
