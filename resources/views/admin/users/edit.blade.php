@extends('layouts.master')

@section('page_title', 'Edit User')

@section('content')

	<h4>Edit User</h4>

	{!! $user->email !!}

	{!! Form::open(['url' => 'admin/users/edit', 'method' => 'POST', 'class' => 'form'] ) !!}

	<div class="form-group">
		{!! Form::hidden('id', $user->id) !!}
		{!! Form::label('role', 'Roles:') !!}
		@foreach($roles as $key => $role)
			<li>{!! $role->name !!} {!! Form::checkbox('role['.$key.']', $role->id, $user->hasRole($role->name)) !!}</li>
		@endforeach
	</div>

	{!! Form::submit('Update', ['class' => 'btn']) !!}
	{!! Form::close() !!}

@stop

@section('javascript')

@stop