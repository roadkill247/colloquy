@extends('layouts.master')

@section('page_title', 'Login')

@section('content')

	{!! Form::open(['url' => 'auth/register', 'method' => 'POST', 'class' => 'form'] ) !!}

	<div class="form-group">
		{!! Form::label('email', 'Email') !!}
		{!! Form::email('email', '', ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('password', 'Password') !!}
		{!! Form::password('password', ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('password_confirmation', 'Confirm Password') !!}
		{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
	</div>

	{!! Form::submit('Submit', ['class' => 'btn']) !!}
	{!! Form::close() !!}

@stop

@section('javascript')

@stop