<?php namespace Colloquy\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

	/**
	 * All of the application's route middleware keys.
	 *
	 * @var array
	 */
	protected $middleware = [
		'auth' 			=> 'Colloquy\Http\Middleware\Authenticated',
		'auth.basic' 	=> 'Colloquy\Http\Middleware\AuthenticatedWithBasicAuth',
		'admin' 		=> 'Colloquy\Http\Middleware\AuthenticatedAdmin',
		'guest' 		=> 'Colloquy\Http\Middleware\IsGuest',

		'manage_users' 	=> 'Colloquy\Http\Middleware\ManageUsers',
		'manage_roles' 	=> 'Colloquy\Http\Middleware\ManageRoles',
	];

	/**
	 * Called before routes are registered.
	 *
	 * Register any model bindings or pattern based filters.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function before(Router $router)
	{
		//
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$router->group(['namespace' => 'Colloquy\Http\Controllers'], function($router)
		{
			require app_path('Http/routes.php');
		});
	}

}
