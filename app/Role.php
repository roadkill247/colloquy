<?php namespace Colloquy;

use Illuminate\Database\Eloquent\Model;
use Colloquy\Permission;

class Role extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'roles';

	/**
     * Validation rules.
     *
     * @var array
     */
	public static $rules = array(
        'name' => 'required|between:4,128'
    );

    /**
     * Get users with a certain role
     */
    public function users()
    {
        return $this->belongsToMany('Colloquy\User');
    }

    /**
     * Get permissions with a certain role
     */
    public function permissions()
    {
        return $this->belongsToMany('Colloquy\Permission');
    }

    /**
     * Get permissions with a certain role
     */
    public function hasPermission($check)
    {
        return in_array($check, array_fetch($this->permissions->toArray(), 'name'));
    }

    /**
     * Find the role by name
     */
    public static function findByName($name)
    {
        return Role::where('name', $name)->first();
    }

}
