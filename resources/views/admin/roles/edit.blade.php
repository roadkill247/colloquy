@extends('layouts.master')

@section('page_title', 'Edit Role')

@section('content')

	<h3>Edit Role</h3>

	<h4>{!! $role->name !!} can:</h4>
	@foreach( $role->permissions as $permission)
		<li>{!! $permission->name !!}</li>
	@endforeach

	<h4>Users who have this role:</h4>
	@foreach($role->users as $user)
		<li>{!! $user->email !!}</li>
	@endforeach

	{!! Form::open(['url' => 'admin/roles/edit', 'method' => 'POST', 'class' => 'form'] ) !!}

	<div class="form-group">
		{!! Form::label('name', 'Name') !!}
		{!! Form::text('name', $role->name, ['class' => 'form-control']) !!}
		{!! Form::hidden('oldname', $role->name) !!}
	</div>

	<div class="form-group">
		{!! Form::label('permissions', 'Permissions:') !!}
		@foreach($permissions as $key => $permission)
			<li>{!! $permission->name !!} {!! Form::checkbox('permission['.$key.']', $permission->id, $role->hasPermission($permission->name)) !!}</li>
		@endforeach
	</div>

	{!! Form::submit('Update', ['class' => 'btn']) !!}
	{!! Form::close() !!}

@stop

@section('javascript')

@stop