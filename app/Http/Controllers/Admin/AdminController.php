<?php namespace Colloquy\Http\Controllers\Admin;

use Illuminate\Contracts\Auth\Guard;
use Colloquy\Http\Controllers\Controller;

class AdminController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Show the admin dashboard
	 *
	 */
	public function getDashboard()
	{
		return view('admin.dashboard');
	}


}
