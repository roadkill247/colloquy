@extends('layouts.master')

@section('page_title', 'Roles')

@section('content')

	<h4>Site Roles</h4>

	<a href="/admin/roles/create">Create Role</a><br />

	<table class="table">
		<thead>
			<th>Role Name</th>
			<th>Role Permissions</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
		@foreach($roles as $role )
			<tr>
				<td>{!! $role->name !!}</td>
				<td>
					@foreach($role->permissions as $permission)
						<li>{!! $permission->name !!}</li>
					@endforeach
				</td>
				{{-- Cannot delete the admin role --}}
				@if($role->name != 'Admin')
					<td><a href="/admin/roles/edit/{!! $role->id !!}">Edit</a></td>
					<td><a href="/admin/roles/delete/{!! $role->id !!}">Delete</a></td>
				@else
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				@endif
			</tr>
		@endforeach
		</tbody>
	</table>

@stop

@section('javascript')

@stop