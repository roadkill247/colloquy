@extends('layouts.master')

@section('page_title', 'Profile')

@section('content')

	<h4>User Profile</h4>

	Email: {!! $user->email !!}<br />
	Created: {!! $user->created_at !!}<br />

	<h4>Roles:</h4>
	@foreach($user->roles as $role)
		<li>{!! $role->name !!}</li>
	@endforeach

	<h4>Permissions:</h4>
	@foreach($permissions as $permission)
		@if($user->can($permission->name))
			<li>{!! $permission->name !!}</li>
		@endif
	@endforeach

	{!! Form::open(['url' => 'user/profile', 'method' => 'POST', 'class' => 'form'] ) !!}

	<div class="form-group">
		{!! Form::label('email', 'Email') !!}
		{!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('password', 'Password') !!}
		{!! Form::password('password', ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('password_confirmation', 'Confirm Password') !!}
		{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
	</div>

	{!! Form::submit('Update', ['class' => 'btn']) !!}
	{!! Form::close() !!}
@stop

@section('javascript')

@stop