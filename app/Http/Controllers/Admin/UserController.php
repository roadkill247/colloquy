<?php namespace Colloquy\Http\Controllers\Admin;

use Illuminate\Contracts\Auth\Guard;
use Colloquy\User;
use Colloquy\Role;
use Colloquy\Permission;
use Colloquy\Http\Controllers\Controller;

class UserController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Show the site users
	 *
	 */
	public function getAll()
	{
		$users = User::all();

		return view('admin.users.index', compact('users'));
	}

	/**
	 * Show the site users
	 *
	 */
	public function getEdit($id)
	{
		$user = User::find($id);
		$roles = Role::all();

		return view('admin.users.edit', compact('user', 'roles'));
	}

	/**
	 * Show the site users
	 *
	 */
	public function postEdit()
	{
		$user = User::find(\Input::get('id'));

		foreach($user->roles as $role){
			$user->roles()->detach($role->id);
		}

		if(\Input::get('role')){
			foreach(\Input::get('role') as $role){
				$user->roles()->attach($role);
			}
		}

		$users = User::all();
		
		return redirect('admin/users/all')->with(compact('users'))
			->withSuccess('User ('.$user->email.') has successfully been updated!');
	}

	/**
	 * Delete a specific user
	 *
	 */
	public function getDelete($id)
	{
		$user = User::find($id);
		$user->delete();

		$users = User::all();

		return redirect('admin/users/all')->with(compact('users'))
			->withSuccess('User has successfully been deleted!');
	}


}
