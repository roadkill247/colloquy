<?php namespace Colloquy\Http\Controllers;

use Colloquy\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Colloquy\User;
use Colloquy\Permission;
use Colloquy\Http\Requests\UserDetailsRequest;

class UserController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth, User $user)
	{
		$this->auth = $auth;
		$this->user = $user;

		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getProfile()
	{
		$user = $this->auth->user();
		$permissions = Permission::all();

		return view('user.profile', compact('user', 'permissions'));
	}

	/**
	 * Update the details for the specific user
	 *
	 * @return Response
	 */
	public function postProfile(UserDetailsRequest $request)
	{
		$user = $this->auth->user();
		$user->email = $request->email;
		$user->password = \Hash::make($request->password);
		$user->save();

		$permissions = Permission::all();

		return redirect('user/profile')->with(compact('user', 'permissions'))
			->withSuccess('Your details have successfully been updated!');
	}

		


}
