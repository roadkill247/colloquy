@extends('layouts.master')

@section('page_title', 'Home')

@section('content')

<h3>Simple Homepage</h3>

@if(Auth::check())
	<div class="row">
		<div class="col-md-12">
			Welcome {!! Auth::user()->email !!},
		</div>
	</div>
@endif

@stop

@section('javascript')

@stop