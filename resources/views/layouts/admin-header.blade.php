@if(Auth::user()->hasRole('Admin'))
	<a href="/admin/dashboard">Admin Dashboard</a> /
@endif

@if(Auth::user()->can('manage_users'))
	<a href="/admin/users/all">Users</a> /
@endif

@if(Auth::user()->can('manage_roles'))
	<a href="/admin/roles/all">Roles</a>		
@endif
