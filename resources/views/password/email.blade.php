@extends('layouts.master')

@section('page_title', 'Forgot Password')

@section('content')

	{!! Form::open(['url' => 'password/email', 'method' => 'POST', 'class' => 'form'] ) !!}

	<div class="form-group">
		{!! Form::label('email', 'Email') !!}
		{!! Form::email('email', '', ['class' => 'form-control']) !!}
	</div>

	{!! Form::submit('Submit', ['class' => 'btn']) !!}
	{!! Form::close() !!}

@stop

@section('javascript')

@stop