<div class="jumbotron">
	<h3 class="text-center">{{ Config::get('site.name') }}</h3>

	<div class="row">
		<div class="col-md-12">
			<span class="pull-left">
				<a href="/">Main Page</a>
			</span>
			<span class="pull-right">
				@if(!Auth::check())
					<a href="/auth/login">Login</a> /
					<a href="/auth/register">Register</a>
				@else
					<a href="/auth/logout">Logout</a>
				@endif
			</span>
		</div>
	</div>

	<div class="row">
		@if(Auth::check())
	        <div class="col-md-12">
	        	@include('layouts.admin-header')
	        	<br />
	            @include('layouts.user-header')
	        </div>
	    @endif
	</div>
	
</div>		