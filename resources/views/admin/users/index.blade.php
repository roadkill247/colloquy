@extends('layouts.master')

@section('page_title', 'Users')

@section('content')

	<h4>Users</h4>

	<table class="table">
		<thead>
			<th>Email</th>
			<th>Roles</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>	
			@foreach($users as $user)
				<tr>
					<td>{!! $user->email !!}</<td>
					<td>
						@foreach($user->roles as $role)
							<li>{!! $role->name !!}</li>
						@endforeach
					</td>
					{{-- You cannot edit/delete your own details --}}
					@if(Auth::user()->id != $user->id)
						<td><a href="/admin/users/edit/{!! $user->id !!}">Edit</a></td>
						<td><a href="/admin/users/delete/{!! $user->id !!}">Delete</a></td>
					@else
					 	<td>&nbsp;</td>
					 	<td>&nbsp;</td>
					@endif
				</tr>
			@endforeach
		</tbody>
	</table>

@stop

@section('javascript')

@stop