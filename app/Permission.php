<?php namespace Colloquy;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'permissions';

    /**
     * Get users with a certain role
     */
    public function roles()
    {
        return $this->belongsToMany('Colloquy\Role');
    }

}
