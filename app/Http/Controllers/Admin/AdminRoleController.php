<?php namespace Colloquy\Http\Controllers\Admin;

use Illuminate\Contracts\Auth\Guard;
use Colloquy\User;
use Colloquy\Role;
use Colloquy\Permission;
use Colloquy\Http\Requests\RoleRequest;
use Colloquy\Http\Controllers\Controller;

class AdminRoleController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Show the site roles 
	 *
	 */
	public function getAll()
	{
		$roles = Role::all();

		return view('admin.roles.index', compact('roles'));
	}

	/**
	 * Show the create roles page
	 *
	 */
	public function getCreate()
	{
		$permissions = Permission::all();

		return view('admin.roles.create', compact('permissions'));
	}

	/**
	 * Create the role 
	 *
	 */
	public function postCreate(RoleRequest $request)
	{
		$role = new Role();
		$role->name = ucfirst($request->name);
		$role->save();

		if($request->permission){
			foreach($request->permission as $permission){
				$role->permissions()->attach($permission);
			}
		}

		$roles = Role::all();

		return redirect('admin/roles/all')->with(compact('roles'))
			->withSuccess('The new role ('.$role->name.') has been created!');
	}

	/**
	 * Edit a specific role
	 *
	 */
	public function getEdit($id)
	{
		$role = Role::find($id);
		$permissions = Permission::all();

		return view('admin.roles.edit', compact('role', 'permissions'));
	}

	/**
	 * Edit a specific role
	 *
	 */
	public function postEdit(RoleRequest $request)
	{
		$role = Role::findByName($request->oldname);
		$role->name = ucfirst($request->name);
		$role->save();

		foreach($role->permissions as $permission)
			$role->permissions()->detach($permission->id);

		if($request->get('permission')){
			foreach($request->get('permission') as $permission)
				$role->permissions()->attach($permission);
		}

		$roles = Role::all();

		return redirect('admin/roles/all')->with(compact('roles'))
			->withSuccess('The role ('.$role->name.') has successfully been updated!');
	}

	/**
	 * Delete a specific role
	 *
	 */
	public function getDelete($id)
	{
		$role = Role::find($id);
		$role->delete();

		$roles = Role::all();

		return redirect('admin/roles/all')->with(compact('roles'))
			->withSuccess('The role has successfully been deleted!');
	}


}
